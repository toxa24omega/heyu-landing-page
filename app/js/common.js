// DROPDOWN LANGUAGE SELECTOR

$(() => {

    const   dropdownLang  = $(`.language`),
            dropdownTitle = $(`.language .title`),
            dropdownMenu  = $(`.language .dropdown-menu`);

    // CLICK ON DROPDOWN
    $(`.language .title`).on(`click`, () => {
        if ($(dropdownMenu).height() > 0) {
            $(dropdownLang).toggleClass(`closed`);
        }

        selectedSimilar();
    });

    // CLICK ON DROPDOWN-LIST ITEM
    $(`.language .dropdown-menu li`).on(`click`, event => {
        let target = event.target;

        changeLang(target);
        $(dropdownLang).toggleClass(`closed`);
    });

    // CLOSE DROPDOWN AFTER CLICK ON DOCUMENT
    $(document).bind(`click`, event => {
        let clicked = $(event.target);
        
        if (!clicked.parents().hasClass(`language`)) {
            $(dropdownLang).addClass(`closed`);
        }
    });

    // CHANGE LANGUAGE
    function changeLang(selectList) {
        let dataLang    = $(selectList).data(`lg`),
            contentList = $(selectList).text();

        $(dropdownTitle).data(`lg`, dataLang);
        $(dropdownTitle).text(contentList);
    }

    // SELECT SIMILAR LANGUAGE
    function selectedSimilar() {
        let listItems = $(`.language .dropdown-menu ul li`),
            itemData  = ``;
        
        for ( let i = 0; i < listItems.length; i++ ) {
            itemData = listItems[i];

            if ($(itemData).data(`lg`) === dropdownTitle.data(`lg`)) {
                $(itemData).addClass(`selected`).siblings().removeClass(`selected`)
            }
        }
    };

});